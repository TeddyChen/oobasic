<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);

namespace OOBasic\Polymorphism\Lsp;

require_once("BasicMath.php");

class TeddyMath extends BasicMath
{
    public function add(int $arg1, int $arg2) : int {
        return $arg1 - $arg2;
    }
}