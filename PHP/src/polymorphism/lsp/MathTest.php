<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);
namespace OOBasic\Polymorphism\Lsp;

require_once("BasicMath.php");
require_once("TeddyMath.php");

use PHPUnit\Framework\TestCase;

class MathTest extends TestCase
{
    /** @test */
    public function BasicMath_add_method_returns_the_sum_of_two_args()
    {
        $math = new BasicMath();
        $this->assertEquals(3, $math->add(1, 2));
    }

    /** @test */
    public function TeddyMath_add_method_returns_the_subtraction_of_two_args()
    {
        $math = new TeddyMath();
        $this->assertEquals(-1, $math->add(1, 2));
    }

}
