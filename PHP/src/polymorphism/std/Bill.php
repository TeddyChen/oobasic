<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

namespace OOBasic\Polymorphism\Std;

require_once("Person.php");

class Bill extends Person
{
    public function talk(): string
    {
        return "我沈默寡言";
    }
}