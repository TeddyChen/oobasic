<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);

namespace OOBasic\Polymorphism\Std;

abstract class Person
{
    private $age;

    public abstract function talk() : string;

    public function setAge(int $age) {
        $this->age = $age;
    }

    public function getAge() : int {
        return $this->age;
    }
}