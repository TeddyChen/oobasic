<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);

namespace OOBasic\Polymorphism\Std;

require_once("MyWinZip.php");
require_once("Rar.php");
require_once("Arj.php");
require_once("Zip.php");

use PHPUnit\Framework\TestCase;

class MyWinZipTest extends TestCase
{
    /** @test */
    public function polymorphism_with_interface_implementation()
    {
        ob_start();

        $myWinZip = new MyWinZip();
        $myWinZip->pack(new Rar());
        $myWinZip->pack(new Arj());
        $myWinZip->pack(new Zip());

        $value = ob_get_contents();
        ob_end_clean();

        $expected = "Using Rar to compress data.\n" .
        "Using Arj to compress data.\n" .
        "Using Zip to compress data.\n";

        $this->assertEquals($expected, $value);
    }

}
