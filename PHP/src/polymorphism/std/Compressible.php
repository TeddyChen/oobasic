<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);

namespace OOBasic\Polymorphism\Std;

interface Compressible
{
    function compress();
}