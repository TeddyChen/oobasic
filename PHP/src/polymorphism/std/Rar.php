<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);

namespace OOBasic\Polymorphism\Std;

require_once("Compressible.php");

class Rar implements Compressible
{
    public function compress()
    {
        printf("Using Rar to compress data.\n");
    }
}