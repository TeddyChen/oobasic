<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);
namespace OOBasic\Polymorphism\Std;

require_once("Teddy.php");
require_once("Bill.php");

use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    /** @test */
    public function polymorphism_with_class_inheritance()
    {
        $person = new Teddy();
        $this->assertEquals("我很會打嘴砲", $person->talk());

        $person = new Bill();
        $this->assertEquals("我沈默寡言", $person->talk());
    }

}
