<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);

namespace OOBasic\Polymorphism\Std;

require_once("Compressible.php");

class MyWinZip
{
    public function pack(Compressible $obj)
    {
        $obj->compress();
    }
}