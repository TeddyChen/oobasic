<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\DuckTyping;

class Teddy
{
    function openClosed()
    {
        printf("Teddy knows OCP.\n");
    }
}