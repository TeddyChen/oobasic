<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\DuckTyping;

require_once("Connection.php");
require_once("Stream.php");
require_once("Door.php");
require_once("Teddy.php");

use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /** @test */
    public function duck_typing_ok_example()
    {
        ob_start();

        $this->closeMeDynamically(new Connection());
        $this->closeMeDynamically(new Stream());
        $this->closeMeDynamically(new Door());

        $value = ob_get_contents();
        ob_end_clean();

        $expected = "Database connection was closed.\n" .
            "IO stream was closed.\n" .
            "The door was closed.\n";

        $this->assertEquals($expected, $value);

    }

    /** @test */
    public function duck_typing_fail_example()
    {
        try{
            $this->closeMeDynamically(new Teddy());
            $this->fail("Infeasible path");
        }
        catch(\Throwable | \Error | \Exception $e){
            $this->assertEquals("Call to undefined method OOBasic\DuckTyping\Teddy::close()",
                $e->getMessage());
        }
    }

	private function closeMeDynamically($object){
	    $object->close();
	}

}
