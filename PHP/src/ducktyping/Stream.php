<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\DuckTyping;

require_once("MyCloseable.php");

class Stream implements MyCloseable
{

    function close()
    {
        printf("IO stream was closed.\n");
    }
}