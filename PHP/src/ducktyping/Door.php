<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\DuckTyping;

class Door
{
    function close()
    {
        printf("The door was closed.\n");
    }
}