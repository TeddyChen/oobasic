<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\Encapsulation;

class PersonData
{
    private $hp;
    private $name = null;

    function __construct()
    {
        $this->hp = 0;
        $this->name = null;
    }

    public function getHp() : int
    {
        return $this->hp;
    }

    public function setHp(int $hp)
    {
        $this->hp = $hp;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
}
