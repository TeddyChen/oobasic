<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\Encapsulation;

class Product {

    public $name;
    private $quantity;
    protected $itemPrice;

    public function __construct()
    {
        $this->quantity = 0;
        $this->itemPrice = 0;
    }

    public static function create(int $quantity, int $itemPrice){
        $instance = new self();
        $instance->setQuantity($quantity);
        $instance->setItemPrice($itemPrice);
        return $instance;
    }

	public function getQuantity() : int
    {
        return $this->quantity;
    }

	public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

	public function getItemPrice() : int
    {
        return $this->itemPrice;
    }

	public function setItemPrice(int $itemPrice)
    {
        $this->itemPrice = $itemPrice;
    }

	public function getPrice()
    {
        return $this->basePrice() * $this->discountFactor();
    }

	private function basePrice()
    {
        return  $this->quantity * $this->itemPrice;
    }

	private function discountFactor()  {
		if ($this->basePrice() > 1000) return 0.88;
        else		return 0.95;
	}
}