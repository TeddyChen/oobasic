<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\Encapsulation;

require_once("ConfigParser.php");
require_once("PersonData.php");

class DBConfigParser extends ConfigParser{

    private $mConnStr = null;

    function __construct(string $connString)
    {
        $this->mConnStr = $connString;
    }

    protected function readData() {
        printf ("Read config data from database: %s\n", $this->mConnStr);
    }

    protected function parseToken() {
        printf ("parseToken from database...\n");
    }

    protected function buildModel() {
        $this->mPData = new PersonData();
        $this->mPData->setName("Kay");
        $this->mPData->setHp(50);
    }

    protected function validate() {
        printf ("validate config data built from database...\n");
    }
}