<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */

declare(strict_types=1);

namespace OOBasic\Encapsulation;

abstract class Shape {
    private $name;

    public function Shape(string $name){
        $this->name = $name;
    }

    public function getName() : string {
            return $this->name;
    }

    public function setName(string $name){
        $this->name = $name;
    }

	public final function getInfo() : string {
		return $this->getName();
	}

	public abstract function area() : double;
}