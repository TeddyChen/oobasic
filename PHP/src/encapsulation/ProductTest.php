<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\Encapsulation;

require_once("Product.php");

use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{

    /** @test */
    public function when_create_new_product_default_quantity_and_item_price_are_zero()
    {
        $product = new Product();

        $this->assertEquals(0, $product->getQuantity());
        $this->assertEquals(0, $product->getItemPrice());
    }

    /** @test */
    public function when_price_less_or_equal_1000_then_gets_95_percent_discount()
    {
        $product = new Product();
        $product->setItemPrice(50);
        $product->setQuantity(20);

        $this->assertEquals(950, $product->getPrice());
    }

    /** @test */
    public function when_price_greater_than_1000_then_gets_88_percent_discount()
    {
        $product = Product::create(50, 200);

        $this->assertEquals(8800, $product->getPrice());
    }

}
