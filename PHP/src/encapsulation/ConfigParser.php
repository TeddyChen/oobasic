<?php
/*
 * Copyright 2018 TeddySoft Technology. All rights reserved.
 *
 */
declare(strict_types=1);

namespace OOBasic\Encapsulation;

require_once("PersonData.php");

abstract class ConfigParser {

    protected $mPData = null;

    public final function doParse() : PersonData {
        $this->readData();
        $this->parseToken();
        $this->buildModel();
        $this->validate();
        return $this->mPData;
    }

    abstract protected function readData();
    abstract protected function parseToken();
    abstract protected function buildModel();
    abstract protected function validate();
}