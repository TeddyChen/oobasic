/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.ducktyping;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import org.junit.Test;

public class DuckTypingTest {
	
	@Test
	public void duck_typing_OK_example() throws Exception {
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);
		
		closeMeDynamically(new Connection());
		closeMeDynamically(new Stream());
		closeMeDynamically(new Door());
	
		String expected = "Database connection was closed.\n" +
				"IO stream was closed.\n" + 
				"The door was closed.\n";
		assertEquals(expected, stream.toString());

	}

	@Test
	public void duck_typing_fail_example() {
		try {
			closeMeDynamically(new Teddy());
		}
		catch (Exception e) {
			assertEquals("java.lang.NoSuchMethodException: tw.teddysoft.oo.ducktyping.Teddy.close()", 
					e.toString());
		}
	}
	
	
	private void closeMeDynamically(Object cls) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Class<?> objClass = cls.getClass();
		objClass.getDeclaredMethod("close").invoke(cls);
	}
}
