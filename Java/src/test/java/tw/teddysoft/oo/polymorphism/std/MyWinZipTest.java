/*
 * Copyright 2017 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.polymorphism.std;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class MyWinZipTest {

	@Test
	public void polymorphism_with_interface_implementation() {
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);
		
		MyWinZip zip = new MyWinZip();
		zip.pack(new Rar());
		zip.pack(new Arj());
		zip.pack(new Zip());
		String expected = "Using Rar to compress data.\n" +
									"Using Arj to compress data.\n" + 
									"Using Zip to compress data.\n";
		assertEquals(expected, stream.toString());
	}
}
