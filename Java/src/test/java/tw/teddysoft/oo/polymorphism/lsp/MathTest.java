package tw.teddysoft.oo.polymorphism.lsp;

import static org.junit.Assert.*;

import org.junit.Test;

public class MathTest {
	private BasicMath math;
	
	@Test
	public void BasicMath_add_method_returns_the_sum_of_two_args() {
		math = new BasicMath();
		assertEquals(3, math.add(1, 2));
	}

	@Test
	public void TeddyMath_add_method_returns_the_subtraction_of_two_args() {
		math = new TeddyMath();
		assertEquals(-1, math.add(1, 2));
	}
}
