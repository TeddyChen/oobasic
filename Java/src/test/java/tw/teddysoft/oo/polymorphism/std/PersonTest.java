/*
 * Copyright 2017 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.polymorphism.std;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class PersonTest {

	@Test
	public void polymorphism_with_class_inheritance() {
		Person person = new Teddy();
		assertEquals("我很會打嘴砲", person.talk());
			
		person = new Bill();
		assertEquals("我沈默寡言", person.talk());
	}
}
