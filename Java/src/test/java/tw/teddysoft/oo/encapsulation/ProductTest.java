/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.encapsulation;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTest {

	@Test
	public void testDiscountFactorIs0_88() {
		Product p = new Product();
		p.setItemPrice(500);
		p.setQuantity(10);
		assertEquals(4400, p.getPrice(), 0.000001);
	}
}
