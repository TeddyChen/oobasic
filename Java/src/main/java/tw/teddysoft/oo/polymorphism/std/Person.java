package tw.teddysoft.oo.polymorphism.std;

public abstract class Person {
	private int age;
	
	public abstract String talk(); 
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public int getAge() {
		return age;
	}
	
}
