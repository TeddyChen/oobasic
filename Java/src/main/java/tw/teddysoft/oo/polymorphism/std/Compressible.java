/*
 * Copyright 2017 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.polymorphism.std;

public interface Compressible {
	void compress();
}
