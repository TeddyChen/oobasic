/*
 * Copyright 2017 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.polymorphism.std;

public class Arj implements Compressible {

	public void compress() {
		System.out.println("Using Arj to compress data.");
	}
}
