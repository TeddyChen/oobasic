/*
 * Copyright 2017 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.polymorphism.std;

public class MyWinZip {
	public void pack(Compressible obj) {
		obj.compress();
	}
}
