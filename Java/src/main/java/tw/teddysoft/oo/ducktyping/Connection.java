package tw.teddysoft.oo.ducktyping;

public class Connection implements MyCloseable {
	@Override
	public void close() {
		System.out.println("Database connection was closed.");
	}

}
