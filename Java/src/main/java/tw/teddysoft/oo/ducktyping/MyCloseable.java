package tw.teddysoft.oo.ducktyping;

public interface MyCloseable {
	void close();
}
