package tw.teddysoft.oo.ducktyping;

public class Stream implements MyCloseable {
	@Override
	public void close() {
		System.out.println("IO stream was closed.");
	}

}
