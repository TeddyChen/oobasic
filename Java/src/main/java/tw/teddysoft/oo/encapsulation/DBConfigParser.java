/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.encapsulation;

public class DBConfigParser extends ConfigParser{
	private String mConnStr = null;
	
	public DBConfigParser(String aConnString){
		super();
		mConnStr = aConnString;
	}
	
	@Override
	protected void readData() {
		System.out.println("Read config data "
				+ "from database: " + mConnStr);
	}
	
	@Override
	protected void parseToken() {
		System.out.println("parseToken...");
	}
	
	@Override
	protected void buildModel() {
		mPData = new PersonData();
		mPData.setName("Kay");
		mPData.setHP(50);
	}
	
	@Override
	protected void validate() {
		System.out.println("validate config data "
				+ "built from database...");
	}
}
