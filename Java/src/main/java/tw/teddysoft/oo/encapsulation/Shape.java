/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.oo.encapsulation;

public abstract class Shape {
	private String name;

	public Shape(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}
  
	public final String getInfo(){
		return this.getName();
	}
	
	public abstract double area();
}
