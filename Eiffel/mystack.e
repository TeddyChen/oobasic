class
	MYSTACK

create
    make

feature
	buffer : ARRAY [STRING]
	count : INTEGER

    make
        do
            create buffer.make_filled ("", 0, 99)
            count := 0
        end

	is_empty : BOOLEAN
		do
			Result := count <= 0;
		end

	top : STRING
		require
			not is_empty
		do
			Result := buffer.at (count-1)
		end

	pop : STRING
		require
			not is_empty
		do
			Result := buffer.at (count-1)
			count := count -1
		ensure
			Result = old top
		end

	push(item : STRING)
		require
			item /= Void
		do
			buffer.put (item, count)
			count := count + 1
		ensure
			top = item
		end

	size : INTEGER
		do
			Result := count
		end
invariant
	count >= 0
end
