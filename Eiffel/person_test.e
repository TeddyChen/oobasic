class PERSON_TEST
inherit TEST_CASE
feature

	person_set_age_ok
		local
			person : PERSON
		do
			create person
			person.set_age (5)
			check person.get_age = 5
		end
	end

	customer_set_age_does_not_violate_LSP
		local
			customer : PERSON
		do
			create {CUSTOMER} customer
			customer.set_age (5)
			check customer.get_age = 5
		end
	end

	rectangle_area_ok
		local
			rectangle : RECTANGLE
		do
			create rectangle.make
			rectangle.set_height (2)
			rectangle.set_width (5)
			check rectangle.area = 10
		end
	end

	square_area_post_condition_violation
		local
			square : RECTANGLE
		do
			create {SQUARE} square.make
			square.set_height (2)
			square.set_width (5)
			check square.area = 10
		end
	end
end
