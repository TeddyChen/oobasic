class RECTANGLE
create
    make
feature
	top_left : POINT
	width : DOUBLE
	height : DOUBLE

    make
        do
            create top_left
            width := 0
            height := 0
        end


	get_width : DOUBLE
		do
			Result := width
	end

	get_height : DOUBLE
		do
			Result := height

	end

	set_width ( width_ : DOUBLE)
		require
			width_ > 0
		do
			width := width_
		ensure
			width = width_
			height = old height
		end

	set_height ( height_ : DOUBLE)
		require
			height_ > 0
		do
			height := height_
		ensure
			height = height_
			width = old width
		end

	area : DOUBLE
		do
			RESULT := width * height;
		ensure
			RESULT = width * height
		end
end
