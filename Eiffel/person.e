class PERSON

feature
	age : INTEGER

	get_age : INTEGER
		do
			Result := age
		ensure
			age >= 0
		end

	set_age ( age_ : INTEGER)
		require
			age_ >= 0
		do
			age := age_
		ensure age = age_
		end

end
