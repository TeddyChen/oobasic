note
	description: "dbc application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			person_test : PERSON_TEST
			stack_test : MYSTACK_TEST
		do
			create person_test

			create stack_test
			--stack_test.push_pop_ok

			--io.put_new_line
			--print(stack.pop)
			print("%N")




			person_test.person_set_age_ok
			person_test.customer_set_age_does_not_violate_LSP

			-- person_test.rectangle_area_ok
			--person_test.square_area_post_condition_violation
			print("%N")
		end
end
