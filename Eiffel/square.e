class SQUARE
inherit RECTANGLE
        redefine
            set_width,
            set_height
        end
create
    make

feature
	set_width ( width_ : DOUBLE)
		require else
			width_ > 0
		do
			width := width_
			height := width
		ensure then
			width = width_
			height = width
		end

	set_height ( height_ : DOUBLE)
		require else
			height_ > 0
		do
			height := height_
			width := height
		ensure then
			height = height_
			width = height
		end
end
