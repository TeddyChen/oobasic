class CUSTOMER
inherit PERSON
        redefine
            set_age
        end
feature

	set_age ( age_ : INTEGER)
		require else
			age_ >= 18
		do
			age := age_
		ensure then age = age_
		end

end
