class
	MYSTACK_TEST
inherit TEST_CASE
feature
	push_pop_ok
		local
			stack : MYSTACK
		do
			create stack.make
			stack.push("Teddy")
			stack.push("Eiffel")
			stack.push("Ada")
			print(stack.top)
			io.put_new_line
			print(equal("Ada", stack.top))
			check
				3 = stack.size
				equal("Ada", stack.top)
			end
			check
				equal("Ada", stack.pop)
				2 = stack.size
				equal("Eiffel", stack.pop)
				1 = stack.size
			end
		end
end
