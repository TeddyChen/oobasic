﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    public class Arj : ICompressible
    {
        public void Compress()
        {
            Console.WriteLine("Using Arj to compress data.");
        }
    }
}
