﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    public abstract class Person
    {
        private int age;

        public abstract String Talk();

        public int Age
        {
            get { return age; }
            set { age = value; }
        }
    }
}
