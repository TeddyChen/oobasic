﻿/*
 * Copyright 2017 TeddySoft Technology.
 * 
 */
using System;
using NUnit.Framework;
using System.IO;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    [TestFixture]
    public class MyWinZipTest
    {
        [Test]
	    public void Polymorphism_with_interface_implementation()
		{
            try
            {
                using (var consoleOut = new StringWriter())
                {
                    Console.SetOut(consoleOut);
                    
                    MyWinZip zip = new MyWinZip();
                    zip.Pack(new Rar());
                    zip.Pack(new Arj());
                    zip.Pack(new Zip());
                    String expected = "Using Rar to compress data.\r\n" +
                                                "Using Arj to compress data.\r\n" +
                                                "Using Zip to compress data.\r\n";
                    Assert.AreEqual(expected, consoleOut.ToString());
                }
            }
            finally
            {
                var standardOutput = new StreamWriter(Console.OpenStandardOutput());
                standardOutput.AutoFlush = true;
                Console.SetOut(standardOutput);
            }
        }
    }
}
