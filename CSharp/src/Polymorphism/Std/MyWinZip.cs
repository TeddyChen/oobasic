﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    public class MyWinZip
    {
        public void Pack(ICompressible obj)
        {
            obj.Compress();
        }
    }
}
