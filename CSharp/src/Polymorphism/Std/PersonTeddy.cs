﻿/*
 * Copyright 2017 TeddySoft Technology.
 * 
 */
using System;
using NUnit.Framework;
using System.IO;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    [TestFixture]
    public class PersonTest
    {
        [Test]
	    public void Polymorphism_with_class_inheritance()
		{
            Person person = new Teddy();
            Assert.AreEqual("我很會打嘴砲", person.Talk());

            person = new Bill();
            Assert.AreEqual("我沈默寡言", person.Talk());
        }
    }
}
