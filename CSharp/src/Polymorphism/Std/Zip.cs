﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    public class Zip : ICompressible
    {
        public void Compress()
        {
            Console.WriteLine("Using Zip to compress data.");
        }
    }
}
