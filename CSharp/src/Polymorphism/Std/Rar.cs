﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    public class Rar : ICompressible
    {
        public void Compress()
        {
            Console.WriteLine("Using Rar to compress data.");
        }
    }
}
