﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    public class Bill : Person
    {
        public override String Talk()
        {
            return "我沈默寡言";
        }
    }
}
