﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Polymorphism.Std
{
    public class Teddy : Person
    {
        public override String Talk()
        {
            return "我很會打嘴砲";
        }
    }
}
