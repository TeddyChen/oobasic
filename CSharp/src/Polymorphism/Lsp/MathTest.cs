﻿/*
 * Copyright 2017 TeddySoft Technology.
 * 
 */
using System;
using NUnit.Framework;
using System.IO;

namespace TW.Teddysoft.OO.Polymorphism.Lsp
{
    [TestFixture]
    public class MathTest
    {
        private BasicMath math;

        [Test]
        public void BasicMath_add_method_returns_the_sum_of_two_args()
        {
            math = new BasicMath();
            Assert.AreEqual(3, math.Add(1, 2));
        }

        [Test]
        public void TeddyMath_add_method_returns_the_subtraction_of_two_args()
        {
            math = new TeddyMath();
            Assert.AreEqual(-1, math.Add(1, 2));
        }
    }
}
