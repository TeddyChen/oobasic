﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Teddysoft.OO.Polymorphism.Lsp
{
    class BasicMath
    {
        public virtual int Add(int arg1, int arg2)
        {
            return arg1 + arg2;
        }
    }
}
