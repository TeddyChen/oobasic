﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Teddysoft.OO.Ducktyping
{
    public class Connection : IMyCloseable
    {
        public void  Close()
        {
            Console.WriteLine("Database connection was closed.");
        }
    }
}
