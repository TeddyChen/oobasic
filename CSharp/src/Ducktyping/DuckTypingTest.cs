﻿/*
 * Copyright 2017 TeddySoft Technology.
 * 
 */
using System;
using NUnit.Framework;
using System.IO;

namespace TW.Teddysoft.OO.Ducktyping
{
    [TestFixture]
    public class DockTypingTest
    {
        [Test]
        public void duck_typing_OK_example()
        {
            try
            {
                using (var consoleOut = new StringWriter())
                {
                    Console.SetOut(consoleOut);

                    CloseMeDynamically(new Connection());
                    CloseMeDynamically(new Stream());
                    CloseMeDynamically(new Door());

                    String expected = "Database connection was closed.\r\n" +
                                      "IO stream connection was closed.\r\n" +
                                      "The door was closed.\r\n";

                    Assert.AreEqual(expected, consoleOut.ToString());
                }
            }
            finally
            {
                var standardOutput = new StreamWriter(Console.OpenStandardOutput());
                standardOutput.AutoFlush = true;
                Console.SetOut(standardOutput);
            }
        }

        [Test]
        public void duck_typing_fail_example()
        {
            try
            {
                CloseMeDynamically(new Teddy());
            }
            catch (Exception e)
            {
                Assert.AreEqual("'TW.Teddysoft.OO.Ducktyping.Teddy' 不包含 'Close' 的定義",
                    e.Message);
            }
        }

        private void CloseMeDynamically(dynamic cls) => cls.Close();
    }
}