﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Teddysoft.OO.Ducktyping
{
    public interface IMyCloseable
    {
        void Close();
    }
}
