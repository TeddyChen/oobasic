﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Teddysoft.OO.Ducktyping
{
    public class Stream : IMyCloseable
    {
        public void Close()
        {
            Console.WriteLine("IO stream connection was closed.");
        }
    }
}
