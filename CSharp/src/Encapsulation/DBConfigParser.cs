﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Encapsulation
{
    public abstract class DBConfigParser : ConfigParser
    {
        private String mConnStr = null;

        public DBConfigParser(String aConnString) : base()
        {
            mConnStr = aConnString;
        }

        protected override void readData()
        {
            Console.WriteLine("Read config data "
                    + "from database: " + mConnStr);
        }

        protected override void parseToken()
        {
            Console.WriteLine("parseToken...");
        }

        protected override void buildModel()
        {
            mPData = new PersonData();
            mPData.Name ="Kay";
            mPData.HP = 50;
        }

        protected override void validate()
        {
            Console.WriteLine("validate config data "
                    + "built from database...");
        }
    }
}
