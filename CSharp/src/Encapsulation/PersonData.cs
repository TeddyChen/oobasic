﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Encapsulation
{
    public class PersonData
    {
        private int _HP = 0;
        private String _name;

        public int HP
        {
            get { return _HP; }
            set { _HP = value; }
        }

        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
