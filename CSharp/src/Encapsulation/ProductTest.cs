﻿/*
 * Copyright 2017 TeddySoft Technology.
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.OO.Encapsulation
{
    [TestFixture]
    public class ProductTest
    {
        [Test]
	    public void testDiscountFactorIs0_88()
		{
            Product p = new Product();
            p.setItemPrice(500);
            p.setQuantity(10);
            Assert.AreEqual(4400, p.getPrice(), 0.000001);
        }
    }
}
