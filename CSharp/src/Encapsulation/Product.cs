﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.OO.Encapsulation
{
    public class Product
    {
        public String name;
        private int quantity;
        protected int itemPrice;

        public Product() { }

        public Product(int quantity, int itemPrice)
        {
            this.quantity = quantity;
            this.itemPrice = itemPrice;
        }

        public int getQuantity() { return quantity; }

        public void setQuantity(int quantity) { this.quantity = quantity; }

        public int getItemPrice() { return itemPrice; }

        public void setItemPrice(int itemPrice) { this.itemPrice = itemPrice; }

        public double getPrice() { return basePrice() * discountFactor(); }

        private double basePrice() { return quantity * itemPrice; }

        private double discountFactor()
        {
            if (basePrice() > 1000) return 0.88;
            else return 0.95;
        }
    }
}
